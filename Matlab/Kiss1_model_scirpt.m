clear all;
Kiss1_model_parameters
params.opt = odeset('MaxStep',1);
params.Dt = 1;plotflag=1;fi=1;
%% 1. run control to steady state OVX
%-68.9241  -70.9198


sec_count = 20;
paramstmp = params; 
paramstmp.Tspan = 0:params.Dt:1000*sec_count;

Xic = zeros(19,1); Xic(15,1) = 1; Xic(17,1) = 1.0;
[T, X0_OVX] = ode45(@(t,y) Kiss1_model(t,y,paramstmp), paramstmp.Tspan , Xic, paramstmp.opt);

if (plotflag)
    figure(fi);  subplot(1,1,1) 
    plot(T/1000-10, X0_OVX(:,1),'k','linewidth', 1.5);
    
    figure(fi*20);  subplot(2,2,1) 
    plot(T/1000, X0_OVX(:,14));
    
end


%% 2. run control to steady state OVX+E2


paramstmp = params_OVXE2; 
paramstmp.opt = odeset('MaxStep',1);
paramstmp.Dt = 1;
paramstmp.Tspan = 0:params.Dt:1000*sec_count;

Xic = zeros(19,1); Xic(15,1) = 1; Xic(17,1) = 1.0;
[T, X0_OVXE2] = ode45(@(t,y) Kiss1_model(t,y,paramstmp), paramstmp.Tspan , Xic, paramstmp.opt);

if (plotflag)
    figure(fi);  subplot(1,1,1) ; hold on;
    plot(T/1000-10, X0_OVXE2(:,1),'r','linewidth', 1.5);
    set(gcf,'paperposition',[0 0 1.6 1]*10);
    set(gca,'fontsize',20);
    xlim([0,10])
    ylim([-80 0])
    print(gcf, 'RMP', '-depsc')

    figure(fi*20);  subplot(2,2,1) ;hold on;
    plot(T/1000, X0_OVXE2(:,14));
    
    
end

disp([X0_OVX(end,1) X0_OVXE2(end,1)]);


