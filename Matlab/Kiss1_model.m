
function dx = Kiss1_model(t, x, params)
    
    %% set up dynamic variables 
 
    %%
    Iapp = params.Iapp;
    slope = params.slope;

    V =  x(1); % Voltage                                                
    h1NaT = x(2); % Fast Sodium Current inactivation variable
    hNaP = x(3); % Persistant Sodium Current inactivation variable
    mA = x(4);  % Transient Potassium Current activation variable
    hA = x(5); % Transient Potassium Current inactivation variable
    mM = x(6); % Delayed-rectifier Potassium activation Current
    h1T = x(7); % Low Voltage Activated Calcium Current inactivation variable
    h2T = x(8); % Low Voltage Activated Calcium Current inactivation variable
    h1h = x(9); % Hyperpolarization Activated Current inactivation variable
    h2h = x(10); % Hyperpolarization Activated Current inactivation variable
    mCa = x(11); % coarse grained Calcium Current activation variable
    hCa = x(12); % coarse grained Calcium Current gating variable
    qgirk = x(13); % GIRK activation variable

    Ca = x(14); % intrcellular calcium
    
    Gin = x(15); % TRPC5 activator - inactive form
    Gact = x(16); %  TRPC5 activator - active form
    
    Gin2 = x(17); % GIRK activator - inactive form
    Gact2 = x(18); % GIRK activator - active form
    I_ramp = x(19);
    if ( I_ramp < 0 ) 
        slope = 0; 
        I_ramp = 0;
    end
    %% Capacitance
    Cm = params.Cm;


    %% reversal potentials
    ENa = params.ENa;
    EK = params.EK;
    ECa = params.ECa;
    Eh = params.Eh;

    %% Transient Na current (I_NaT)
    %parameters
    gNaT = params.gNaT;
    mNaTVh = params.mNaTVh;
    mNaTk = params.mNaTk;
    hNaTVh = params.hNaTVh;
    hNaTk = params.hNaTk;
    hNaTtau = params.hNaTtau;
    
    % current
    minfNaT=1.0/(1.0+exp((mNaTVh-V)/mNaTk));
    hinfNaT=1.0/(1.0+exp((hNaTVh-V)/hNaTk));
    INaT = gNaT*minfNaT*h1NaT*(V-ENa);

    %% Persistent Na current (I_NaP)
    % parameters
    gNaP = params.gNaP;
    mNaPVh = params.mNaPVh;
    mNaPk = params.mNaPk;
    hNaPVh = params.hNaPVh;
    hNaPk = params.hNaPk;
    tauhNaP = params.tauhNaP;

    % current
    minfNaP=1.0/(1.0+exp((mNaPVh-V)/mNaPk));
    hinfNaP=1.0/(1.0+exp((hNaPVh-V)/hNaPk));
    INaP = gNaP*minfNaP*hNaP*(V-ENa);

    %% A current (I_A)
    %parameters
    gA = params.gA;
    mAVh = params.mAVh;
    mAk = params.mAk;
    hAVh = params.hAVh;
    hAk = params.hAk;
    taumA = params.taumAp;
    tauhA = params.tauhAp;

    %current
    minfA=1.0/(1.0+exp((mAVh-V)/mAk));
    hinfA=1.0/(1.0+exp((hAVh-V)/hAk));
    IA = gA*mA*hA*(V-EK);

    %% parameters for BK channels
    %parameters
    gBK = params.gBK;
    K_BK = params.K_BK;
    V_BK0 = params.V_BK0;
    k_shift = params.k_shift;
    k_ca_BK = params.k_ca_BK;

    %current
    if (Ca>0)
        V_BK = V_BK0 - k_shift * log(Ca/k_ca_BK);
        act_BK =  1/ (1 + exp( - (V - V_BK )/ K_BK) );
    else
        act_BK = 0;
    end

    I_BK = gBK * act_BK * (V-EK);


    %% parameters for SK channels
    %parameters
    gSK = params.gSK;
    K_ca = params.K_ca;
    n_ca = params.n_ca;
    
    %current
    omega  = Ca.^n_ca ./ (Ca.^n_ca + K_ca.^n_ca); %1./(1+exp(- (Ca - K_ca)*4*n_ca )); ;%;%1/(1+exp(- (Ca - 0.01)/.1 ));%Ca.^n_ca ./ (Ca.^n_ca + K_ca.^n_ca);
    I_SK = gSK * omega * (V-EK);


    %% M current (I_M)
    %parameters
    gM = params.gM;
    mMVh = params.mMVh;
    mMk = params.mMk;
    mMtau = params.mMtau;
    
    %current
    minfM=1.0/(1.0+exp((mMVh-V)/mMk));
    IM = gM*mM*(V-EK);
    
    %% h current (I_h)
    %parameters
    gh = params.gh;
    hhVh = params.hhVh;
    hhk1 = params.hhk1;
    hhk2 = params.hhk2;
    ph = params.ph;
    hhtau1 = params.hhtau1;
    hhtau2 = params.hhtau2;
    
    %current
    hinf1h=1.0/(1.0+exp((hhVh-V)/hhk1));
    hinf2h=1.0/(1.0+exp((hhVh-V)/hhk2));

    Ih = gh*(ph*h1h+(1-ph)*h2h)*(V-Eh);
    %% Low threshold Ca current (I_T)
    %parameters
    gT = params.gT;
    mTVh = params.mTVh;
    mTk = params.mTk;
    hTVh = params.hTVh;
    hTk = params.hTk;
    hTtau1 = params.hTtau1;
    hTtau2 = params.hTtau2;
    hTf = params.hTf;
    
    %current
    minfT=1.0/(1.0+exp((mTVh-V)/mTk));
    hinf1T=1.0/(1.0+exp((hTVh-V)/hTk));
    hinf2T=1.0/(1.0+exp((hTVh-V)/hTk));
    IT = gT*minfT*(hTf*h1T+(1-hTf)*h2T)*(V-ECa);

    %% coarse-grained Ca channel (I_coarse)
    %parameters
    gCa = params.gCa;
    mCaVh = params.mCaVh;
    mCaK = params.mCaK;
    mCatau= params.mCatau;
    hCaVh = params.hCaVh;
    hCaK = params.hCaK;
    hCatau = params.hCatau;
    
    %current
    minfCa=1.0/(1.0+exp((mCaVh-V)/mCaK));
    hinfCa=1.0/(1.0+exp((hCaVh-V)/hCaK));

    ICa = gCa*mCa*hCa*(V-ECa);
      

    %% TRPC5 current (I_TRPC5)

    %parameters
    gTRPC5 = params.gTRPC5;
    Ca_trpc = params.Ca_trpc;
    kCa_trpc = params.kCa_trpc ;
    
    %current
    T_nkb = 1/(1+exp(- (Ca - Ca_trpc )/kCa_trpc )) ;    
    ITRPC5 = gTRPC5  * T_nkb  * (V-params.Etrpc5) *  Gact ;

    %% parameters GIRK channel 

    %parameters
    gGIRK = params.gGIRK;
    V_GIRK = params.V_GIRK; 
    alpha = params.alpha;
    alpha_t = params.alpha_t; 
    beta_t = params.beta_t; 
    kGIRK1 =   params.kGIRK1; 
    kGIRK2 =   params.kGIRK2; 
   
    %current
    q_inf = (1./(1 + exp((V-V_GIRK)/kGIRK1))+ alpha./(1 + exp((V-V_GIRK)/kGIRK2)));
    q_tau = 1./(alpha_t.*exp(-V./V_GIRK) + beta_t.*exp(V./V_GIRK) );

    I_GIRK = gGIRK * qgirk * (V - EK) * Gact2 ;


    %% leak currents
    gleakCa = params.gleakCa;
    VleakCa = params.VleakCa ;
    kleakCa = params.kleakCa ;
    gleak = params.gleak;
    Eleak = params.Eleak;
    I_leak = gleak*(V-Eleak);
    I_leak_Ca = gleakCa*1.0/(1.0+exp((VleakCa-V)/kleakCa))*(V-ECa);
    
    %% other model parameters 

    NKB = params.NKB;
    KN = params.KN;
    gamma = params.gamma;
    d_ca = params.d_ca;
    k_trpc5_0 = params.k_trpc5_0;
    k_trpc5 = params.k_trpc5;
    k_mtrpc5 = params.k_mtrpc5;
    S = params.GIRK_a;
    KS = params.KS;
    k_GIRK = params.k_GIRK;
    k_mGIRK = params.mk_GIRK;

    
    %% model equations
    dx = nan(size(x));
    dx(1) = ( I_ramp + Iapp - (I_leak + INaT + INaP + IA + IM + IT + I_SK + I_BK + I_GIRK + I_leak_Ca + ...
        ICa + Ih + ITRPC5  ))/Cm;

    % inactivation gating variable for NaT channel
    dx(2) =  (hinfNaT - h1NaT)/hNaTtau;

    % inactivation gating variable for NaP channel
    dx(3) = (hinfNaP - hNaP)/tauhNaP;

    % A current gating variables
    dx(4) = (minfA - mA)/taumA;
    dx(5) = (hinfA - hA)/tauhA;

    % M current
    dx(6) = (minfM - mM)/mMtau;

    % T current gating variable
    dx(7) = (hinf1T - h1T)/hTtau1;
    dx(8) = (hinf2T - h2T)/hTtau2;

    % h current gating variable
    dx(9)  = (hinf1h - h1h)/hhtau1;
    dx(10) = (hinf2h - h2h)/hhtau2;
  
    % coarse grained ca2+ channel gating variable
    dx(11)  = (minfCa - mCa)/mCatau;
    dx(12)  = (hinfCa - hCa)/hCatau;

    % GIRK activation 
    dx(13) = (q_inf - qgirk)/q_tau;

    %intracellular caclium 
    dx(14) =  -(0.9*ITRPC5 + IT + ICa + I_leak_Ca ) * gamma - d_ca*Ca   ;
   
    % TRPC5 activation
    act_rate = k_trpc5_0 + k_trpc5*NKB^2/(KN^2 + NKB^2);
    dx(15) = - act_rate * Gin  + k_mtrpc5 * Gact ;
    dx(16) =   act_rate * Gin  - k_mtrpc5 * Gact  ;
     
    % GIRK activation
    act_rate = k_GIRK*S^2/(KS^2 + S^2);
    dx(17) = - act_rate * Gin2   ;
    dx(18) =   act_rate * Gin2  - k_mGIRK * Gact2 ;

    dx(19) = slope; 
    
end
