%% Capacitance
params.Cm = 23.13;
 
%% reversal potentials
params.ENa = 66.10;
params.EK = -81;
params.ECa = 121.6;
params.Etrpc5 = -15;
params.Eh = -27.8;

%% Transient Na current (I_NaT)
params.gNaT = 90;
params.mNaTVh= -25;
params.mNaTk = 7;
params.hNaTVh = -43.2;
params.hNaTk = -2;
params.hNaTtau =4.5;
    
    
%% Persistent Na current (I_NaP)
% parameters
params.gNaP = 3.3700;
params.mNaPVh = -41.5000;
params.mNaPk = 3;
params.hNaPVh = -47.4000;
params.hNaPk = -8.2000;
params.tauhNaP = 250;

    
%% A current (I_A)
%parameters
params.gA = 60;6.3656;
params.mAVh = -30;
params.mAk = 10;
params.hAVh = -55.1;
params.hAk = -11.4;
params.taumAp = 10;
params.tauhAp = 20;

%% parameters for BK channels
%parameters
params.gBK = 13.5;
params.K_BK = 10;
params.V_BK0 = 0.1;
params.k_shift = 18;
params.k_ca_BK = 1.5;

%% parameters for SK channels
%parameters
params.gSK = 28.13;
params.K_ca = 0.74;
params.n_ca = 2.2;
    

%% M current (I_M)
%parameters
params.gM = 0.23;
params.mMVh = -50;
params.mMk = 20;
params.mMtau = 10;
    

%% h current (I_h)
%parameters
params.gh = 0.5617;
params.hhVh = -102;
params.hhk1= -10;
params.hhk2 = -10;
params.ph = 0.85;
params.hhtau1 = 80;
params.hhtau2 = 310;
    
   
%% Low threshold Ca current (I_T)
%parameters
params.gT = 0.6660;
params.mTVh = -54;
params.mTk = 3.3;
params.hTVh = -69.1;
params.hTk = -5.3;
params.hTtau1 = 1.94;
params.hTtau2 = 86.3;
params.hTf = 0.5;
    
   
%% coarse-grained Ca channel (I_coarse)
%parameters
params.gCa = 2.1;
params.mCaVh = -27.3;
params.mCaK = 4.62;
params.mCatau = 10;
params.hCaVh = -48.9;
params.hCaK = -18.2;
params.hCatau = 300;
    
   
%% TRPC5 current (I_TRPC5)
%parameters
params.gTRPC5 = 8.4;
params.Ca_trpc = 0.6;
params.kCa_trpc = 1/3;0.333;
    
    
%% parameters GIRK channel 
%parameters
params.gGIRK = 0.;
params.V_GIRK = 67.0828;
params.alpha = 0.8;
params.alpha_t = 0.0061; 
params.beta_t = 0.0818;
params.kGIRK1 = 20; 
params.kGIRK2 = 100;
   
%% leak currents

params.gleak = 3.5;
params.Eleak = -81;
params.gleakCa = 4;
params.VleakCa = 20;
params.kleakCa = 10;

    
%% other model parameters 
params.gamma = 1.9631e-05;
params.d_ca = 0.008;

params.NKB = 0;
params.KN = 32;
params.k_trpc5_0 = 0.002;
params.k_trpc5 = 0.006;
params.k_mtrpc5 = 0.002;

params.GIRK_a = 0;
params.KS = 1;
params.k_GIRK = 0.001;
params.mk_GIRK = 3.33e-6;


params.Iapp=0;
params.slope=0;
%% OVX+E2 params    
params_OVXE2 = params;
params_OVXE2.gT = 4.995;
params_OVXE2.gCa = 2.8;
params_OVXE2.gh = 11.2340;
params_OVXE2.gTRPC5 = 1.68;
params_OVXE2.gBK = 20;
params_OVXE2.Cm =  19.5;
params_OVXE2.gA =   35;
params_OVXE2.gleak = 3.65;
params_OVXE2.gM = 1.23;